FROM python:3
ENV PYTHONUNBUFFERED 1
RUN mkdir /code
COPY . /code/
WORKDIR /code

RUN apt-get update && apt-get install -y --no-install-recommends \
        curl \
        libc6-dev \
        build-essential \
        postgresql-client \
        netcat \
    && apt-get clean
RUN curl -LO https://www.tbi.univie.ac.at/RNA/download/sourcecode/2_4_x/ViennaRNA-2.4.11.tar.gz \
    && tar -xf ViennaRNA-2.4.11.tar.gz \
    && cd ViennaRNA-2.4.11 \
    && ./configure --with-python3 \
    && make \
    && make install \
    && rm ../ViennaRNA-2.4.11.tar.gz
RUN pip install --upgrade pip && pip install -r requirements.txt

ENTRYPOINT ["/code/entrypoint.sh"]
