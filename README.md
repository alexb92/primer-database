Documentation of Oligo Primer Database

Assignment:

Erstellen Sie eine einfache Datenbankanwendung (SQLite, MariaDb, PostgresDb) zur Ver- waltung von Primersequenzen. Das Programm soll das Anlegen, Löschen und Suchen von Primersequenzen über die Commandline ermöglichen.
    • Optional: Zusätzlich kann das Programm um eine Webkomponente erweitert werden, dass die Verwaltung der Datenbank über ein Webservice (z.B. Django, Flask) ermöglicht.
    • Optional: Erweitern Sie das Programm so, dass man nach einer Primersequenz in der Datenbank blasten kann.
    • Zu speichernde Felder: (Oligo Name, Sequence, Scale (mmol), Notes (Optional), Order will be blocked for review, Date of Order, Oligo Number(internal Id), Ordered by, Template type, Application, Storage location)

Mögliche Module (argparse, sqlite3, SQLAlchemy, Flask, Django, subprocess, biopython)


Programs/ Modules used:

    • Python3
    • Django
    • HTML5
    • Javascript
    • CSS
    • Docker
    • PostgreSQL
    • Gitlab


**Procedure/ Structure:**

**Installation**

The project was started by installing Docker and Django and subsequent creation of a new project. The database format used is PostgreSQL, as defined in the settings and the docker file.

A new app was created (DB) and added to the project.  

A new class  “Primer” was defined in the models.py in the DB app, containing the requested attribute fields for data entries.

Datamigration was performed to set up SQL database. A superuser was added to access admin panel of project.

Configuration of Admin Site

The admin site of the newly created project was configured to show the requested fields in a table view. A search bar functionality was added to filter entries.


Configuration of User Sites - General

The url for the main table view of the user site was called .../primer/. A check for user authentication was implemented in views.py by redirecting every request to the newly created user login window if not authenticated. 

If user is already logged in and user login window is opened, buttons to redirect to Admin Panel (if user has superuser status) and Database are shown.

**Configuration of User Views**

    1. PrimerView
PrimerView defines the main table view of the database. It has a display limit of 25 entries per page. Most fields are shown in this table, the only exclusions are fields that have a specific purpose elsewhere.
Columns can be ordered by clicking on the column name. Generally, the table is ordered by its primary key (ascending).
The name of each entry is linked to the entry-specific DetailView, where entries can be changed (to a certain degree).
The primer sequence that is assigned to each entry will automatically generate a scalable vector graphic (svg) of the predicted secondary structure. This svg image is also linked to an entry-specific View, the StructureView. 

An option to export the whole or filtered table to excel is available by pressing the button “export”.  

The search bar in the top left defines a request for which the table will be filtered. If no search request is given, the whole table will be displayed.
The search options can be configured by clicking on the cogwheel left of the search bar.
Search Options:
    • Filter by text
    • Filter by number
	Additional Options:
    • All fields that contain text can be chosen to be excluded from the search by checking the associated checkbox.
    • A date range can be determined for either the date of creation or the date the primer was ordered last.
    • If only the from-date is given, the till-date will be automatically set to the current date.
    • If only the till-date is given, the from-date will be automatically set to 01.01.2000.

    2. NewEntryView
NewEntryView can be reached by clicking the button “new”. An empty entry form will be displayed.
The validity of the data will be checked automatically. 
The sequence of the primer will generate a new .svg of the secondary structure.
The melting temperature will automatically be calculated using the “nearest-neighbour”-model. Results may be different than by other calculators found online, so caution is advised.
The length of the sequence will be calculated automatically.

Fields:
    • The date of creation is automatically set to the current date but can be changed. 
    • “Created by” will automatically be populated with the current users username.
    • Primer Group is defaulted to “Ungrouped” but can be changed to any other previously created group. This can also be done retroactively in DetailView.
    • The modifications can be chosen from a fixed list and are defaulted to “None”.
      If the modification lists need to be changed, this has to be done in the program directory.
    • The date of last order can not be changed while creating a new entry, this has to be automatically done by the admin in the OrderView or manually for each entry in the DetailView.
    • If the Primer does not have the status “ordered”, a message will be displayed.
    • The PO Number can’t be changed in the NewEntryView, this has to be done in DetailView by an admin.
    • “Lock record” will lock the sequence, the date of creation and the creator for this entry. This can only be reversed by an admin in the admin panel.


    3. DetailView
DetailView can be reached by clicking on the name of a specific entry. It will be filtered to only show the data specific to that entry (by primary key). 
The validity of the data will be checked automatically. 
If the sequence of a primer is changed, a new .svg of the secondary structure will be generated.
The melting temperature will automatically be calculated using the “nearest-neighbour”-model. Results may be different than by other calculators found online, so caution is advised.
The length of the sequence will be calculated automatically.

Fields:
    • Normal users can’t make changes to the field “PO_nr”. It can only be changed by admins.
    • The “last order date” can’t be changed by normal users. It can only be changed by admins.
    • If the record is locked,  the sequence, the date of creation and the creator for this entry cannot be changed anymore. This can only be reversed by an admin in the admin panel.

    4. StructureView
StructureView can be reached by clicking on the generated image of the secondary structure of an entry. The same image will be displayed at 800x800px instead of 100x100px.
To edit the .svg, Element Tree xml is used. The .svg is converted from a string into a tree view. Height and width of the image are changed. Afterwards the tree view is converted back to a string and displayed.
An option to change the svg image into a png image is available by clicking on the button “png”. Contrary to the svg, the png image can be copied or saved freely.
       
    5. Groupview
GroupView can be reached by clicking on the button “Groups” in the main view.
The primer groups are an additional table with groups that can be added by authorized users. Groups are meant to be used to show connections between primers. GroupView also keeps track of how many primer are associated with a group and what their names are.
By clicking on the name of a specific group, GroupFilterView can be reached.
       
    6. GroupFilterView
GroupFilterView can be reached by clicking on the name of a group in the GroupView. It will display all primers that are associated with this certain group.
       
An option to export the whole table to excel is available by pressing the button “export”.
       
    7. OrderView
OrderView can only be accessed by admins. It shows all entries per default and can be filtered by the status of the field “to be ordered”.
Fields that are selected with the checkbox next to each entry can be set to “to be ordered” = True or False at the same time. Additionally, if the view is filtered to “Checked”, an export of the filtered table can be downloaded.
Once the primers have been ordered, the button “Order” can be used to set all primers with “to be ordered” = True to “to be ordered” = False and “Ordered” = True. This action will also change the “last ordered” date to the current day.
       
	
**Python Moduls:**

    1. ViennaRNA
The ViennaRNA package allows for a wide range of commands for predictions and comparison of RNA secondary structures. In this context it is used to generate a scalable vector graphic of each oligo primer sequence.
The commands used are RNA.fold and RNA.svg_rna_plot.

    2. Django-tables2
Django-tables2 is an app to automatically generate HTML tables. In this context it is used to generate the main table in PrimerView.
