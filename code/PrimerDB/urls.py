from django.contrib import admin
from django.urls import path, include
from django.conf.urls import url
from django.views.generic.base import RedirectView


urlpatterns = [
    url('primer/admin/', admin.site.urls),
    path('primer/', include('primer.urls')),
    path('', RedirectView.as_view(url='/primer/', permanent=False), name='index'),
    path('primer/users/', include('django.contrib.auth.urls')),
]
