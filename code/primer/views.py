from django.db.models import Q
from django.contrib.postgres.search import SearchVector
from django.http import HttpResponse, HttpResponseRedirect
from .models import Primer, PrimerGroup, OrderHistory, validate_sequence
from .forms import PrimerForm, PrimerGroupForm, OrderForm, OrderHistoryForm, UploadFileForm
from django.shortcuts import render, redirect
from django.core.files import File
from .tables import PrimerTable, PrimerGroupTable, OrderTable, ExportTable, OrderHistoryTable
from django_tables2 import RequestConfig
import RNA
from pathlib import Path
import xml.etree.ElementTree as ET
import datetime
from django_tables2.export.export import TableExport
from cairosvg import svg2png
from django.contrib.auth.models import User
from datetime import date
import primer3
import pandas as pd
import os
from django.db import transaction, DatabaseError

##### FUNCTIONS #####

def Back(request):
    return redirect(os.environ['PRIMER_BACK'])


def search_request(request, search_vector):
    if "q" in request.GET and request.GET["q"]:
        q = request.GET["q"]
        datefrom = str(request.GET["from"])
        if datefrom == "":
            datefrom = "2000-01-01"
        datetill = str(request.GET["till"])
        if datetill == "":
            datetill = str(date.today())
        if "switch" in request.GET:
            switch = request.GET["switch"]
        else:
            switch = None

        # Searches for q in all specified columns
        # Differentiate between numbers and characters
        if q.isdigit():
            if switch != "ordered":
                qfloat = float(q)
                table = Primer.objects.filter(Q(scale=qfloat) & Q(
                    datefirst__range=[datefrom, datetill]) | Q(oligonr=qfloat) & Q(
                        datefirst__range=[datefrom, datetill]) | Q(melttemp = qfloat) & Q(
                        datefirst__range=[datefrom, datetill]) | Q(length = qfloat) & Q(
                        datefirst__range=[datefrom, datetill]) | Q(group = qfloat) & Q(
                        datefirst__range=[datefrom, datetill]))

            else:
                qfloat = float(q)
                table = Primer.objects.filter(Q(scale=qfloat) & Q(
                    datelast__range=[datefrom, datetill]) | Q(oligonr=qfloat) & Q(
                    datelast__range=[datefrom, datetill]) | Q(melttemp=qfloat) & Q(
                    datelast__range=[datefrom, datetill]) | Q(length=qfloat) & Q(
                    datelast__range=[datefrom, datetill]) | Q(group=qfloat) & Q(
                    datelast__range=[datefrom, datetill]))

        if not q.isdigit():
            if switch != "ordered":
                qlower = q.lower()
                table = Primer.objects.annotate(
                    search=search_vector).filter(Q(search__contains=qlower) & Q(
                    datefirst__range=[datefrom, datetill]) | Q(search=qlower) & Q(
                    datefirst__range=[datefrom, datetill]))
            else:
                qlower = q.lower()
                table = Primer.objects.annotate(
                    search=search_vector).filter(Q(search__contains=qlower) & Q(
                    datelast__range=[datefrom, datetill]) | Q(search=qlower) & Q(
                    datelast__range=[datefrom, datetill]))


        return q, table, datefrom, datetill, switch

    elif "q" in request.GET and request.GET["q"] == "":
        datefrom = str(request.GET["from"])
        datetill = str(request.GET["till"])

        if datefrom == "" and datetill == "":
            datetill = None
            datefrom = None
        else:
            if datefrom == "":
                datefrom = "2000-01-01"
            if datetill == "":
                datetill= str(date.today())

        q = None
        if "switch" in request.GET:
            switch = request.GET["switch"]
            if switch != "ordered":
                table = Primer.objects.filter(Q(datefirst__range=[datefrom, datetill]))
            else:
                table = Primer.objects.filter(Q(datelast__range=[datefrom, datetill]))
        else:
            switch = None
            table = Primer.objects.all()

        return q, table, datefrom, datetill, switch


def file_download(orderfile, filepath = "staticfiles/media/"):
    path_to_file = Path(filepath).joinpath(orderfile)
    f = open(path_to_file, 'rb')
    myfile = File(f)
    content = "vnd.ms-excel"
    if orderfile.endswith("pdf"):
        content = "pdf"
    response = HttpResponse(myfile, content_type='application/{}'.format(content))
    response['Content-Disposition'] = 'attachment; filename=' + orderfile
    return response


def file_upload(request):
    error = None
    form = UploadFileForm()

    if request.method == 'POST':
        form = UploadFileForm(request.POST, request.FILES)
        if form.is_valid():
            df_upload = pd.read_excel(request.FILES['file'], header=0)
            # CHECKS
            # check if provided template was used
            column_list = ['oligoname', 'sequence', "mods5", 'mods3', 'scale',
                           'templatetype', 'application', 'storagelocation', 'notes']
            missing_columns = [i for i in column_list if i not in df_upload.columns.values.tolist()]
            if len(missing_columns) > 0:
                error = "Please make sure to use the provided Excel template!"
                return render(request, 'primer/uploadview.html', {'form': form, 'error': error, 'missing': missing_columns})

            # check if any Primernames are already in Database
            exist_names = [i for i in df_upload["oligoname"].values.tolist() if i in Primer.objects.values_list("oligoname",flat=True)]
            if len(exist_names) > 0:
                error = "At least one Primer in the list already exists with the same name in the Database. Please change the name accordingly or check the Database!"
                return render(request, 'primer/uploadview.html', {'form': form, 'error': error, 'names': exist_names})

            # check if modifications are allowed
            mod5 = open("staticfiles/primer/files/modification_5'.txt")
            mod5 = [i.strip() for i in mod5.readlines()]
            mod3 = open("staticfiles/primer/files/modification_3'.txt")
            mod3 = [i.strip() for i in mod3.readlines()]

            if not all(x in mod5 for x in df_upload["mods5"].values.tolist()):
                error = "At least one 5' modification in the list does not comply with given modification list. A list of allowed 5' modifications can be found in the excel template!"
                return render(request, 'primer/uploadview.html', {'form': form, 'error': error})

            if not all(x in mod3 for x in df_upload["mods3"].values.tolist()):
                error = "At least one 3' modification in the list does not comply with given modification list. A list of allowed 3' modifications can be found in the excel template!"
                return render(request, 'primer/uploadview.html', {'form': form, 'error': error})

            # check if scale is correct
            if not all(x >= 0.001 for x in df_upload["scale"].values.tolist()):
                error = "At least one scale value in the list is smaller than 0.001. Please set it to a value higher than 0.001!"
                return render(request, 'primer/uploadview.html', {'form': form, 'error': error})

            # check for invalid characters
            allowed = ["A", "C", "G", "T", "*", "B", "D", "H", "K", "M", "N", "R", "S", "V", "W", "Y", "-"]
            for index, item in enumerate(df_upload["sequence"].values.tolist()):
                if not all(x in allowed for x in df_upload["sequence"][index]):
                    error = "Some characters in one of the sequences are not allowed. Allowed characters:" \
                            "A, C, G, T, *, B, D, H, K, M, N, R, S, V, W, Y, -"
                    return render(request, 'primer/uploadview.html', {'form': form, 'error': error})

            # WRITE
            # after initial checks, try to write into database
            try:
                with transaction.atomic(): # writing to database is one collective action
                    for i in range(0, len(df_upload)):
                        df_input = df_upload.loc[df_upload.index == i]
                        seq = df_input["sequence"][i]
                        if "-" in seq:
                            # melting temperature
                            df_input["melttemp"] = round(primer3.calcTm(seq.split("-")[1]), 2)
                        else:
                            df_input["melttemp"] = round(primer3.calcTm(seq), 2)

                        seq.replace("-","")

                        # SVG
                        ss, mfe = RNA.fold(seq)
                        # Creating temporary svg.file
                        filename = Path.cwd() / "tmp.svg"
                        RNA.svg_rna_plot(seq, ss, str(filename))
                        # Resizing svg to 100x100px to fit table by replacing code
                        with open(filename, "rb") as f:
                            svg = _add_view_box(f.read(), 100, 100)
                        # Deletion of temporary svg.file
                        filename.unlink()
                        df_input["secstruc"] =  svg

                        # ENTRY INTO DATABASE
                        newprimer = Primer(createdby= request.user, datefirst = date.today(), datelast = None, orderblockedforreview = False,
                                           tbo = False, ordered = False, lockrec = False, purification = "Desalt", format = "Dry")
                        newprimer.oligoname = df_input["oligoname"][i]
                        newprimer.sequence = df_input["sequence"][i].upper()
                        newprimer.mods5 = df_input["mods5"][i]
                        newprimer.mods3 = df_input["mods3"][i]
                        newprimer.scale = df_input["scale"][i]
                        newprimer.templatetype = df_input["templatetype"][i]
                        newprimer.application = df_input["application"][i]
                        newprimer.storagelocation = df_input["storagelocation"][i]
                        newprimer.notes = df_input["notes"][i]
                        newprimer.melttemp = df_input["melttemp"][i]
                        newprimer.length = len(seq)
                        newprimer.secstruc = df_input["secstruc"][i]
                        newprimer.save()

            # Error message
            except (ValueError, TypeError) as e:
                error = f"Databaseerror in row {i}: {e} "
                return render(request, 'primer/uploadview.html', {'form': form, 'error': error, 'names': exist_names})

            # Close window
            return HttpResponse('<script type="text/javascript">window.close()</script>')

    # File download
    elif request.GET:
        if "download" in request.GET:
            return file_download("upload_template.xlsx", filepath="staticfiles/primer/files")


    return render(request, 'primer/uploadview.html', {'form': form, 'error': error})


def _add_view_box(svg, width, height):
    """
    Add a viewBox attribute and new height and width to the <svg> tag
    :param svg: str
    :param width: str
    :param height: str
    :return: str (svg)
    """
    # Important to use a standard namespace
    ET.register_namespace("", "http://www.w3.org/2000/svg")

    root = ET.fromstring(svg)

    # Get original height and width
    w = root.attrib["width"]
    h = root.attrib["height"]

    # Set viewBox
    root.attrib["viewBox"] = "0 0 {width} {height}".format(width=w, height=h)
    root.attrib["width"] = "{}px".format(width)
    root.attrib["height"] = "{}px".format(height)

    svg = ET.tostring(root, encoding="unicode", method="html")

    return svg


def resize_svg(svg, width, height):
    """
    Resize svg
    :param svg: str
    :param width: str
    :param height: str
    :return: str (svg)
    """
    # Important to use a standard namespace
    ET.register_namespace("", "http://www.w3.org/2000/svg")

    root = ET.fromstring(svg)

    # Resize
    root.attrib["width"] = "{}px".format(width)
    root.attrib["height"] = "{}px".format(height)

    svg = ET.tostring(root, encoding="unicode", method="html")

    return svg


##### VIEWS #####

def PrimerGroupView(request):
    form = PrimerGroupForm(request.POST or None)
    table = PrimerGroup.objects.all()
    table = PrimerGroupTable(table, order_by="groupnr")

    if form.is_valid():
        try:
            int(form["name"])
        except:
            form.save()
        return redirect("primer:groups")

    RequestConfig(request, paginate={"per_page": 25}).configure(table)
    return render(request, "primer/primergroupview.html",{"form": form, 'table': table})


def PrimerGroupNew(request):
    form = PrimerGroupForm(request.POST or None)

    if form.is_valid():
        form.save()
        return HttpResponse('<script type="text/javascript">window.close()</script>')

    return render(request, "primer/primergroupform.html", {"form": form})


def Filtergroup(request,name):
    temp = PrimerGroup.objects.get(name=name)
    filter = temp.groupnr
    table = Primer.objects.filter(Q(group=filter))

    export_format = request.GET.get('_export', None)
    if TableExport.is_valid_format(export_format):
        table = PrimerTable(table, order_by="oligonr")
        exporter = TableExport(export_format, table, exclude_columns=('secstruc','purification', 'format',
            'select'))
        return exporter.response('Primers_Group_{}.{}'.format(name, export_format))

    table = PrimerTable(table, order_by="oligonr", exclude=('purification', 'format',
                'concentration', 'select', 'tubenumber','ordered'))
    return render(request, "primer/primergroupfilter.html", {"table": table, 'name':name})


def StructureView(request, pk):
    svg = Primer.objects.filter(pk=pk).first()
    structure = resize_svg(svg.secstruc, 700, 700)
    bytestring = bytes(structure, 'UTF-8')
    fn = "staticfiles/primer/images/output.png"
    svg2png(bytestring, write_to=fn)

    return render(request, "primer/structureview.html", {"structure": structure})


def OrderView(request):
    if request.user.is_superuser:
        table = Primer.objects.all()
        add_form = OrderForm(request.POST or None, initial={"add": "True", "remove": "False"})
        remove_form = OrderForm(request.POST or None, initial={"add": "False", "remove": "True"})
        q, datefrom,  datetill, switch, errmsg = None, None, None, None, None
        today = str(date.today())

        if request.GET:
            add_form = OrderForm(request.POST or None, initial={"add": "True", "remove": "False"})
            remove_form = OrderForm(request.POST or None, initial={"add": "False", "remove": "True"})

            if "q" in request.GET and request.GET["q"] or "switch" in request.GET:
                # declaring all fields that are searched in q as a list
                allfields = ['oligoname', 'sequence', 'mods5', 'mods3', 'createdby', 'templatetype',
                             'application', 'storagelocation', 'notes']
                # declaring all fields that the user can search
                searchfields = ['oligoname', 'sequence', 'mods5', 'mods3', 'createdby', 'templatetype',
                                'application', 'storagelocation', 'notes']

                # removing fields the user does not want to search
                for i in allfields:
                    if i in request.GET:
                        searchfields.remove(i)

                # building custom search vector from searched fields
                for i in searchfields:
                    if 'searchvec' in locals():
                        searchvec += SearchVector(i)
                    else:
                        searchvec = SearchVector(i)

                # defining error message display, resetting searchvec if searchfields is empty
                errmsg = ""
                if len(searchfields) == 0:
                    searchvec = SearchVector('oligoname', 'sequence', 'mods5', 'mods3', 'createdby', 'templatetype',
                                             'application', 'storagelocation', 'notes', )
                    errmsg = "Error: You can't check all boxes!"

                q, table, datefrom, datetill, switch = search_request(request, searchvec)


            if 'f' in request.GET and not "q" in request.GET:
                if request.GET["f"] == "true":
                    table = Primer.objects.filter(tbo="True")
                else:
                    table = Primer.objects.filter(tbo="False")

            if 'f' in request.GET and "q" in request.GET:
                if request.GET["f"] == "true":
                    table = table.filter(tbo="True")
                else:
                    table = table.filter(tbo="False")

            export_format = request.GET.get('_export', None)
            if TableExport.is_valid_format(export_format):  # Export orderlist format to Excel file
                exp_table = Primer.objects.filter(tbo="True")
                exp_table = ExportTable(exp_table, order_by="oligonr" )

                # Checking for existing Modifications, on True other columns will be exported
                modslist = []
                for i in Primer.objects.filter(tbo=True).values_list("mods5",flat=True):
                    modslist.append(i)
                for i in Primer.objects.filter(tbo=True).values_list("mods3",flat=True):
                    modslist.append(i)
                if all(x == "None" for x in modslist):
                        exporter = TableExport(export_format, exp_table, exclude_columns=('oligonr', 'length', 'secstruc',
                                                                                      'datefirst', 'datelast',
                                                                                      'createdby', 'group',
                                                                                      'orderblockedforreview',
                                                                                      'melttemp',
                                                                                      'templatetype', 'application',
                                                                                      'storagelocation', 'tbo',
                                                                                      'ordered', 'lockrec', 'select'))

                else:
                    exporter = TableExport(export_format, exp_table, exclude_columns=('oligonr', 'length', 'secstruc',
                                                                                  'datefirst', 'datelast', 'createdby',
                                                                                  'group', "mods5", 'mods3',
                                                                                  'orderblockedforreview', 'melttemp',
                                                                                  'templatetype', 'application',
                                                                                  'storagelocation', 'tbo', 'ordered',
                                                                                  'lockrec', 'purification', 'format',
                                                                                  'concentration', 'select',
                                                                                  'tubenumber'))

                return exporter.response('Primer_Orderform_{}.{}'.format(date.today(), export_format))

            add_form = OrderForm(request.POST or None, initial={"add": "True", "remove": "False"})
            remove_form = OrderForm(request.POST or None, initial={"add": "False", "remove": "True"})


        if request.POST:
            print(request.POST)
            add_form = OrderForm(request.POST or None, initial={"add": "True", "remove": "False"})
            remove_form = OrderForm(request.POST or None, initial={"add": "False", "remove": "True"})

            # Creating a new entry in Order History if selected Primers are ordered
            if 'order' in request.POST and len(Primer.objects.filter(tbo=True).values_list("oligoname")) > 0:

                # Checking which template to use
                modslist = []
                for i in Primer.objects.filter(tbo=True).values_list("mods5",flat=True):
                    modslist.append(i)
                for i in Primer.objects.filter(tbo=True).values_list("mods3",flat=True):
                    modslist.append(i)

                # PANDAS
                if all(x == "None" for x in modslist):
                    df_table = pd.DataFrame(list(Primer.objects.filter(tbo=True).values().order_by('oligonr')))
                    df_table = df_table[["oligoname", "sequence", "scale", "notes"]]
                    df_table = df_table.rename(index=str,
                                               columns={"oligoname": "Oligo Name", "sequence": "Sequence",
                                                        "scale": "Scale (mmol)", "notes": "Notes (Optional)"})
                else:
                    df_table = pd.DataFrame(list(Primer.objects.filter(tbo=True).values().order_by('oligonr')))
                    df_table = df_table[["oligoname", "mods5", "sequence", "mods3", "scale", "purification", "format", "notes"]]
                    df_table["Concentration (mM)"] = ""
                    df_table["Number of Tubes\n(Default = 1tube)"] = ""
                    df_table = df_table[["oligoname","mods5", "sequence", "mods3", "scale", "purification", "format",
                                         "Concentration (mM)","Number of Tubes\n(Default = 1tube)", "notes"]]
                    df_table = df_table.rename(index=str,
                                               columns={"oligoname": "Oligo Name", "sequence": "Sequence",
                                                        "scale": "Scale (mmol)", "notes": "Notes (Optional)",
                                                        "mods5": "Modifications 5'", "mods3": "Modifications 3'",
                                                        "purification": "Purification", "format": "Format"})

                # Replacing the hyphen in the Sequence
                for i in df_table["Sequence"].values:
                    df_table["Sequence"].loc[df_table["Sequence"] == i] = i.replace("-", "")
                # Starting index with 1 instead of 0
                new_index = [i for i in range(1,len(df_table)+1)]
                df_table.index = new_index

                # If duplicates are created
                if Path.cwd().joinpath("staticfiles/media/Primer_Orderform_{}.xlsx".format(date.today())).exists():
                    xlsx = pd.ExcelWriter("staticfiles/media/Primer_Orderform_{}(2).xlsx".format(date.today()))
                    filename = "mediafiles/Primer_Orderform_{}(2).xlsx".format(date.today())
                else:
                    xlsx = pd.ExcelWriter("staticfiles/media/Primer_Orderform_{}.xlsx".format(date.today()))
                    filename = "mediafiles/Primer_Orderform_{}.xlsx".format(date.today())

                for i in range(2, 10):
                    if Path.cwd().joinpath("staticfiles/media/Primer_Orderform_{}({}).xlsx".format(date.today(),i)).exists():
                        xlsx = pd.ExcelWriter("staticfiles/media/Primer_Orderform_{}({}).xlsx".format(date.today(),i+1))
                        filename = "mediafiles/Primer_Orderform_{}({}).xlsx".format(date.today(),i+1)

                # Write to excel and save file
                df_table.to_excel(xlsx,"sheet1")
                xlsx.save()

                # Filter for correct entries
                setnrs = Primer.objects.filter(tbo=True).values_list("oligonr",flat=True).order_by('oligonr')
                if len(setnrs) != 0:
                    # Create a new entry in OrderHistory
                    temp_history_entry = OrderHistory(orderdate=datetime.date.today())
                    primer_list = ""

                    # Change values in filtered Primer objects accordingly
                    for i in setnrs:
                        primerset = Primer.objects.get(pk=i)
                        primerset.ordered = True
                        primerset.tbo = False
                        primerset.datelast = date.today()
                        primerset.save()
                        if primer_list == "":
                            primer_list += str(primerset.oligoname)
                        else:
                            primer_list += ", " + str(primerset.oligoname)

                    # Assign values to new entry
                    temp_history_entry.orderlen = len(primer_list.split(","))
                    temp_history_entry.primers = primer_list
                    temp_history_entry.orderfile = filename
                    temp_history_entry.save()

            if add_form.is_valid() and remove_form.is_valid():
                mylist = request.POST["list"]
                if request.POST["add"] == "True":
                    for i in mylist.split(","):
                        order = Primer.objects.get(pk=int(i))
                        order.tbo = True
                        order.ordered = False
                        order.save()
                else:
                    for i in mylist.split(","):
                        order = Primer.objects.get(pk=int(i))
                        order.tbo = False
                        order.save()

                if request.GET:
                    return redirect("/primer/order")

            if "addall" in request.POST:
                for i in table:
                    primerset = Primer.objects.get(oligoname=i)
                    primerset.tbo = True
                    primerset.ordered = False
                    primerset.save()
                return redirect("/primer/order")

            if "removeall" in request.POST:
                for i in table:
                    primerset = Primer.objects.get(oligoname=i)
                    primerset.tbo = False
                    primerset.save()
                return redirect("/primer/order")


            add_form = OrderForm(request.GET or None, initial={"add": "True", "remove": "False"})
            remove_form = OrderForm(request.GET or None, initial={"add": "False", "remove": "True"})


        if request.POST and request.GET:
            add_form = OrderForm(request.POST or None, initial={"add": "True", "remove": "False"})
            remove_form = OrderForm(request.POST or None, initial={"add": "False", "remove": "True"})

        table = OrderTable(table,
                           order_by="oligonr",
                           exclude=('secstruc', 'storagelocation', 'templatetype', 'application', 'melttemp', 'length', 'purification','format'))

        return render(request, "primer/orderview.html",
                      {"table": table, "q": q, "errmsg": errmsg, 'datefrom': datefrom, 'datetill': datetill,
                       'today': today, 'switch': switch, "add_form": add_form, "remove_form": remove_form})

    else:
        return redirect("/primer")


def OrderHistoryView(request):
    if request.user.is_superuser:
        table = OrderHistory.objects.all()
        today = str(date.today())
        datefrom = None
        datetill = None
        q = None

        #Search request
        if "q" in request.GET and request.GET["q"]:
            q = request.GET["q"]
            datefrom = str(request.GET["from"])
            if datefrom == "":
                datefrom = "2000-01-01"
            datetill = str(request.GET["till"])
            if datetill == "":
                datetill = str(date.today())

            try:
                qint = int(q)
                print(type(qint))
                table = OrderHistory.objects.filter(Q(ordernr=qint) & Q(
                    orderdate__range=[datefrom, datetill]) | Q(orderlen=qint) & Q(
                    orderdate__range=[datefrom, datetill]))
            except:
                print("exception")
                searchvec = SearchVector("PO_nr", "primers")
                qlower = q.lower()
                table = OrderHistory.objects.annotate(
                    search=searchvec).filter(Q(search__contains=qlower) & Q(
                    orderdate__range=[datefrom, datetill]) | Q(search=qlower) & Q(
                    orderdate__range=[datefrom, datetill]))

        elif "q" in request.GET and request.GET["q"] == "":
            datefrom = str(request.GET["from"])
            datetill = str(request.GET["till"])

            if datefrom == "" and datetill == "":
                datetill = None
                datefrom = None
                table = OrderHistory.objects.all()
            else:
                if datefrom == "":
                    datefrom = "2000-01-01"
                if datetill == "":
                    datetill = str(date.today())
                table = OrderHistory.objects.filter(Q(orderdate__range=[datefrom, datetill]))

            #Export search request
            export_format = request.GET.get('_export', None)
            if TableExport.is_valid_format(export_format):
                table = OrderHistoryTable(table, order_by="ordernr", exclude="orderfile")
                exporter = TableExport(export_format, table)
                return exporter.response('Orderlist_{}_{}.{}'.format(q, date.today(), export_format))

        # Export all
        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            table = OrderHistoryTable(table, order_by="ordernr", exclude="orderfile")
            exporter = TableExport(export_format, table)
            return exporter.response('Orderlist_{}.{}'.format(date.today(), export_format))

        table = OrderHistoryTable(table, order_by="ordernr")

        RequestConfig(request, paginate={"per_page": 50}).configure(table)
        return render(request, "primer/orderhistoryview.html", {"table":table, "q": q, 'datefrom': datefrom, 'datetill': datetill, 'today':today})

    else:
        return redirect("/primer")


def OrderHistoryFormView(request, pk):
    if request.user.is_superuser:
        order = OrderHistory.objects.get(pk=pk)
        print(order.orderfile)
        if request.POST:
            form = OrderHistoryForm(request.POST, request.FILES, instance=order)
            corr_file = str(order.orderfile).replace("mediafiles",  "staticfiles/media")
            oldfile = Path.cwd().joinpath(corr_file)
            if oldfile == Path.cwd():
                filechange = False
            else:
                filechange = True

            if form.is_valid():
                if filechange == True:
                    if "orderdate" in form.changed_data or "primers" in form.changed_data:
                        pass
                    else:
                        oldfile.unlink()
                        form.save()
                    return redirect("primer:orderhistory")
                else:
                    if "orderdate" in form.changed_data or "primers" in form.changed_data:
                        pass
                    else:
                        form.save()
                    return redirect("primer:orderhistory")

        else:
            form = OrderHistoryForm(request.POST or None, instance=order)

            return render(request, "primer/orderhistoryform.html", {"form": form, "nr": pk})


    else:
        return redirect("/primer")


def NewEntryView(request):
    if request.user.is_authenticated:
        form = PrimerForm(request.POST or None, initial={'createdby': request.user, "datefirst": datetime.date.today(), 'scale': 1, "group": 1})
        form.fields["datelast"].disabled = True

        # Validation of data and subsequent generation of svg from input sequence
        if form.is_valid():
            # new_primer as temporary entry
            new_primer = form.save(commit=False)
            form = form.cleaned_data

            if "-" in form["sequence"]:
                seq = form["sequence"].replace("-","").upper()
            else:
                seq = form["sequence"].upper()
            new_primer.sequence = form["sequence"].upper()

            # Commands from ViennaRNA module to generate secondary structure
            ss, mfe = RNA.fold(seq)
            # Creating temporary svg.file
            filename = Path.cwd() / "tmp.svg"
            RNA.svg_rna_plot(seq, ss, str(filename))
            # Resizing svg to 100x100px to fit table by replacing code

            with open(filename, "rb") as f:
                svg = _add_view_box(f.read(), 100, 100)

            # Deletion of temporary svg.file
            filename.unlink()
            new_primer.secstruc = svg

            # Calculating length of primer
            new_primer.length = len(seq)

            # Calculating Melting Temperature of Primer
            validator = ["A", "C", "G", "T", "-"]
            if "-" in form["sequence"]:
                new_primer.melttemp = round(primer3.calcTm(new_primer.sequence.split("-")[1]), 2)
            else:
                new_primer.melttemp = round(primer3.calcTm(new_primer.sequence), 2)
            for i in new_primer.sequence:
                    if i not in validator:
                        new_primer.melttemp = 0
                        break

            # Saving data from temporary entry
            new_primer.save()

            # Look up names and count of linked primers in PrimerGroup
            primer_group = PrimerGroup.objects.get(groupnr=new_primer.group.groupnr)
            primer_group.primer_cnt = Primer.objects.filter(group=new_primer.group).count()
            primers = Primer.objects.values_list('oligoname',flat=True).filter(group=new_primer.group).order_by('oligonr')
            primer_group.primernames = ", ".join(primers)
            primer_group.save()

            return redirect("primer:primer")

        return render(request, "primer/newentryview.html", {"form": form})

    else:
        return redirect("/primer/users/login")


def DetailView(request, pk):
    if request.user.is_authenticated:
        instance = Primer.objects.get(pk=pk)

        # Getting 'createdby' value for current entry and checking for the corresponding ID in user list
        form = Primer.objects.get(pk=pk)
        users  = User.objects.get(username=form.createdby)
        usernr = users.id
        groups = PrimerGroup.objects.get(name=form.group)
        prigroup = groups.groupnr
        groupname = groups.name

        # Setting user ID as initial for 'createdby' field
        form = PrimerForm(request.POST or None, instance=instance, initial={'createdby': usernr, 'group': prigroup})

        # Checking for admin status for last order date_nr
        if not request.user.is_superuser:
            form.fields["datelast"].disabled = True

        # Disabling certain fields after primer has been locked
        if "checked" in str(form["lockrec"]):
            form.fields["sequence"].disabled = True
            form.fields["createdby"].disabled = True
            form.fields["datefirst"].disabled = True

        # Validation of data and subsequent generation of svg from input sequence
        if form.is_valid():
            # new_primer as temporary entry
            new_primer = form.save(commit=False)
            form = form.cleaned_data

            if "-" in form["sequence"]:
                seq = form["sequence"].replace("-","")
            else:
                seq = form["sequence"]
            new_primer.sequence = form["sequence"].upper()

            # Commands from ViennaRNA module to generate secondary structure
            ss, mfe = RNA.fold(seq)
            # Creating temporary svg.file
            filename = Path.cwd() / "tmp.svg"

            RNA.svg_rna_plot(seq, ss, str(filename))

            # Resizing svg to 100x100px to fit table
            with open(filename, "rb") as f:
                svg = _add_view_box(f.read(), 100, 100)

            # Deletion of temporary svg.file
            filename.unlink()

            new_primer.secstruc = svg
            # Calculating length of primer
            new_primer.length = len(seq)
            # Calculating Melting Temperature of Primer
            validator = ["A", "C", "G", "T", "-"]
            if "-" in form["sequence"]:
                new_primer.melttemp = round(primer3.calcTm(new_primer.sequence.split("-")[1]), 2)
            else:
                new_primer.melttemp = round(primer3.calcTm(new_primer.sequence), 2)
            for i in new_primer.sequence:
                    if i not in validator:
                        new_primer.melttemp = 0
                        break
            # tbo and ordered cannot be true at the same time
            if new_primer.tbo == True:
                new_primer.ordered = False

            if new_primer.ordered == True:
                new_primer.tbo = False

            # Saving data from temporary entry
            new_primer.save()
            # If linked group has been changed, old and new group's count will be changed accordingly
            if new_primer.group != groupname:
                primer_group1 = PrimerGroup.objects.get(groupnr=new_primer.group.groupnr)
                primer_group1.primer_cnt = Primer.objects.filter(group=new_primer.group).count()
                primers = Primer.objects.values_list('oligoname', flat=True).filter(group=new_primer.group).order_by(
                    'oligonr')
                primer_group1.primernames = ", ".join(primers)
                primer_group1.save()

                primer_group2 = PrimerGroup.objects.get(groupnr=prigroup)
                primer_group2.primer_cnt = Primer.objects.filter(group=prigroup).count()
                primers = Primer.objects.values_list('oligoname', flat=True).filter(group=prigroup).order_by(
                    'oligonr')
                primer_group2.primernames = ", ".join(primers)
                primer_group2.save()

            return redirect("primer:primer")

        return render(request, "primer/detailview.html", {"form": form, "nr": pk})

    else:
        return redirect("/primer/users/login")


def PrimerView(request):
    # Unlinks output.png from images, if existing
    if Path().cwd().joinpath('staticfiles/primer/images/output.png').exists():
        Path().cwd().joinpath('staticfiles/primer/images/output.png').unlink()

    # Download help file
    if "download" in request.GET:
        return file_download("user_handbook.pdf", filepath="staticfiles/primer/files")

    # declaring all fields that are searched in q as a list
    allfields = ['oligoname', 'sequence', 'mods5', 'mods3','createdby', 'templatetype',
        'application', 'storagelocation', 'notes']
    # declaring all fields that the user can search
    searchfields = ['oligoname', 'sequence', 'mods5', 'mods3', 'createdby', 'templatetype',
        'application', 'storagelocation', 'notes']

    # removing fields the user does not want to search
    for i in allfields:
        if i in request.GET:
            searchfields.remove(i)

    # building custom search vector from searched fields
    for i in searchfields:
        if 'searchvec' in locals():
            searchvec += SearchVector(i)
        else:
            searchvec = SearchVector(i)

    # defining error message display, resetting searchvec if searchfields is empty
    errmsg = ""
    if len(searchfields) == 0:
        searchvec = SearchVector('oligoname', 'sequence','mods5', 'mods3','createdby', 'templatetype',
        'application', 'storagelocation', 'notes',)
        errmsg = "Error: You can't check all boxes!"

    today = str(date.today())

    # Run search function if applicable
    if search_request(request, searchvec):
        q, table, datefrom, datetill, switch = search_request(request, searchvec)

        # Export table with search parameters to Excel file
        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            table = PrimerTable(table,order_by="oligonr")
            exporter = TableExport(export_format, table, exclude_columns=('secstruc','purification', 'format',
            'select'))
            return exporter.response('Primers_{} - {}.{}'.format(datefrom,datetill, export_format))

    else:
        # If no search request is given, whole table will be displayed
        table = Primer.objects.all()
        try:
            group = PrimerGroup.objects.get(name="Ungrouped")
        except:
            PrimerGroup.objects.create(name="Ungrouped")
        q = None
        datefrom = None
        datetill = None
        switch = None

        # Export whole table to Excel file
        export_format = request.GET.get('_export', None)
        if TableExport.is_valid_format(export_format):
            table = Primer.objects.all()
            table = PrimerTable(table,order_by="oligonr")
            exporter = TableExport(export_format, table, exclude_columns=('secstruc','purification', 'format',
            'select'))
            return exporter.response('PrimerTable.{}'.format(export_format))

    table = PrimerTable(table, order_by="oligonr", exclude=('ordered','purification','format'))
    RequestConfig(request, paginate={"per_page": 50}).configure(table)

    return render(request, "primer/tableview.html", {"table": table, "q": q, "errmsg":errmsg, 'datefrom': datefrom,'datetill': datetill, 'today':today,'switch':switch})
