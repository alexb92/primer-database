import django_tables2 as tables
from .models import Primer, PrimerGroup, OrderHistory
from django_tables2.utils import A
from django.utils.safestring import mark_safe

class ImageColumn(tables.Column):
    #Prevent masking of generated svg code
    def render(self, value):
        return mark_safe(value)

class ExtraColumn(tables.Column):
    def render(self, value):
        return value

class PrimerTable(tables.Table):
    datefirst = tables.DateColumn(format ='d M Y')
    datelast = tables.DateColumn(format='d M Y')
    #Creating linked columns to switch to specific sites -> detail, structure
    oligoname = tables.Column(linkify=("primer:detail", (tables.A("pk"),)))
    secstruc = ImageColumn(linkify=("primer:structure", (tables.A("pk"),)))

    class Meta:
        model = Primer
        template_name = 'django_tables2/bootstrap4.html'


class PrimerGroupTable(tables.Table):
    name=tables.URLColumn()
    class Meta:
        model = PrimerGroup
        template_name = 'django_tables2/bootstrap4.html'


class CustomColumn(tables.TemplateColumn):
    attrs = {"td": {"id": lambda record: record.pk} }

class OrderTable(tables.Table):
    oligoname = tables.Column(linkify=("primer:detail", (tables.A("pk"),)))
    datefirst = tables.DateColumn(format ='d M Y')
    datelast = tables.DateColumn(format='d M Y')
    select = CustomColumn('<input type="checkbox" class="checkbox" value=""/> ', verbose_name="Select")

    class Meta:
        model = Primer
        template_name = 'django_tables2/bootstrap4.html'
        sequence = ('select','oligonr','oligoname', 'tbo','ordered','datelast', 'orderblockedforreview')

class ExportTable(OrderTable):
    concentration = tables.Column("Concentration (mM)")
    tubenumber = tables.Column("Number of Tubes\n(Default = 1tube)")

    class Meta:
        sequence = ('oligoname', 'mods5','sequence','mods3','scale','purification','format','concentration','tubenumber','notes')

    def render_sequence(self, value):
        """
        Replacing any hyphen that are in sequence on exporting the table
        :param value: str
        :return: str
        """
        return value.replace("-","")

    def _render_mods(self, value):
        """
        Replaces "None" with an empty field
        :param value: str
        :return:
        """
        if value == "None":
            value = ""
        return value

    def render_mods3(self, value):
        return self._render_mods(value)

    def render_mods5(self, value):
        return self._render_mods(value)

class OrderHistoryTable(tables.Table):
    ordernr = tables.Column(linkify=("primer:orderhistoryform", (tables.A("pk"),)))
    orderdate = tables.DateColumn(format='d M Y')
    orderfile = tables.URLColumn()

    class Meta:
        model = OrderHistory
        template_name = 'django_tables2/bootstrap4.html'