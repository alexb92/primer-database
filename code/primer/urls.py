from django.urls import path
from . import views

app_name = 'primer'
urlpatterns = [
    path("back/", views.Back, name='backto'),
    path('',  views.PrimerView, name='primer'),
    path("<int:pk>/", views.DetailView, name='detail'),
    path('new/', views.NewEntryView, name="new"),
    path('<int:pk>/structure/', views.StructureView, name="structure"),
    path('primergroups/', views.PrimerGroupView, name="groups"),
    path('primergroupform/', views.PrimerGroupNew, name="groupform"),
    path("order/", views.OrderView, name='order'),
    path("orderhistory/", views.OrderHistoryView, name='orderhistory'),
    path("<int:pk>/orderhistory/", views.OrderHistoryFormView, name='orderhistoryform'),
    path("upload/", views.file_upload, name = "fileupload"),
    path("<name>/", views.Filtergroup, name='filtergroup'),
    path("mediafiles/<orderfile>/", views.file_download, name= 'download'),
]
