from django.contrib import admin
from .models import Primer, PrimerGroup, OrderHistory
from django.shortcuts import render

# Register your models here.

class PrimerAdmin(admin.ModelAdmin):
    list_display = ('oligonr','oligoname', 'sequence', 'scale', 'orderblockedforreview','datefirst','datelast','createdby','templatetype','application','storagelocation','lockrec')
    ordering = ['oligonr']
    list_filter = ['datefirst']
    search_fields = ('oligoname', 'sequence','orderdate','createdby','templatetype','application','storagelocation')

    #Definition of search to use search bar in admin view
    def get_search_results(self, request, queryset, search_term):
        queryset, use_distinct = super().get_search_results(request, queryset, search_term)
        try:
            search_term_as_int = int(search_term)
        except ValueError:
            pass
        return queryset, use_distinct


admin.site.register(Primer,PrimerAdmin)
admin.site.register(PrimerGroup)
admin.site.register(OrderHistory)
