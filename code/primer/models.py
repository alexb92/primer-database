from django.db import models
import datetime
from django.core.validators import MinValueValidator
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def validate_non_int(value):
    if value.isdigit():
        raise ValidationError(
            _('Groupname must contain at least one non-numeric character!')
        )

def validate_sequence(value):
    allowed = ["A","C","G","T","*","B","D","H","K","M","N","R","S","V","W","Y","-"]
    forbidden = []
    for i in str(value).upper():
        if i not in allowed and i not in forbidden:
            forbidden.append(i)
    if len(forbidden) > 0:
        forbidden.sort()
        raise ValidationError(
            _('The following characters are not allowed: {}'.format(forbidden)),
        )
    matches = [x for x in value if x == "-"]
    if len(matches) > 1:
        raise ValidationError(
            _('Hyphens are only allowed once. Currently there are {} in the sequence!'.format(len(matches))),
        )


class PrimerGroup(models.Model):
    groupnr = models.AutoField("Group Nr.", primary_key=True)
    name = models.CharField("Group Name", max_length=150, blank=False, unique=True, validators=[validate_non_int])
    primer_cnt = models.IntegerField("Nr. of Linked Primers", default=0)
    primernames = models.CharField("Linked Primer Names", blank = False, default="None", max_length= 10000)

    def __str__(self):
        return self.name

class Primer(models.Model):
    oligonr = models.AutoField("Nr.", primary_key=True)
    oligoname = models.CharField("Oligo Name",max_length= 300, blank=False, unique=True)
    sequence = models.TextField("Sequence", max_length= 220, blank=False, validators=[validate_sequence])
    length = models.IntegerField("Length", blank=True, default=1)
    melttemp = models.FloatField("Melting Temp.", blank=True)
    secstruc = models.CharField("Secondary Structure", max_length= 2000000,blank=True)
    mods5 = models.CharField("Modifications 5'", blank = True, max_length=200, default=None)
    mods3 = models.CharField("Modifications 3'",  blank = True, max_length=200, default=None)
    scale = models.FloatField("Scale (mmol)", validators= [MinValueValidator(0.001)])
    createdby = models.CharField("Created by", max_length= 400, default=None)
    datefirst = models.DateField("Created", default=datetime.date.today)
    datelast = models.DateField("Ordered last", null=True, blank=True)
    group = models.ForeignKey(PrimerGroup, on_delete=models.DO_NOTHING, blank=True, null=True, default=1, related_name="primers")
    orderblockedforreview = models.BooleanField("Order blocked for Review", default=False)
    templatetype = models.CharField("Template type", max_length=400, blank=True)
    application = models.CharField("Application", max_length=400, blank=True)
    storagelocation = models.CharField("Storage Location", max_length=400, blank=True)
    notes = models.CharField("Notes (Optional)", max_length=1000, blank=True)
    tbo = models.BooleanField("To be ordered", default=False)
    ordered = models.BooleanField("Ordered", default=False)
    lockrec = models.BooleanField("Locked", default=False)
    purification = models.CharField("Purification", blank=True, max_length=30, default="Desalt")
    format = models.CharField("Format", blank=True, max_length=30, default="Dry")


    def __str__(self):
        return self.oligoname


class OrderHistory(models.Model):
    ordernr = models.AutoField("Order Number", primary_key=True)
    PO_nr= models.CharField("PO Number", blank=True, max_length=1000)
    orderdate = models.DateField("Date of Order", blank = True)
    orderlen = models.IntegerField("Amount of Primers", blank=True, default=1)
    primers = models.CharField("Ordered Primers", blank = True, max_length=10000)
    orderfile = models.FileField("Excel File", blank = True, upload_to="staticfiles/media/")
