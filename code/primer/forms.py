from .models import Primer,PrimerGroup, OrderHistory
from django import forms
from django.forms.widgets import SelectDateWidget, FileInput
from datetime import date
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError
from django.utils.translation import gettext_lazy as _

def validator_excel(value):
    temp = str(value)
    if not temp.endswith("xls") or temp.endswith("xlsx"):
        raise ValidationError(
            _('Filetype must be Excel!')
        )

mod5 = open("staticfiles/primer/files/modification_5'.txt")
mod5 = [i.strip() for i in mod5.readlines()]
mod5_choices = zip(mod5, mod5)

mod3 = open("staticfiles/primer/files/modification_3'.txt")
mod3 = [i.strip() for i in mod3.readlines()]
mod3_choices = zip(mod3, mod3)


class OrderForm(forms.Form):
    list = forms.CharField(max_length=100000,widget=forms.HiddenInput())
    add = forms.CharField(max_length=6,widget=forms.HiddenInput())
    remove = forms.CharField(max_length=6,widget=forms.HiddenInput())


class PrimerForm(forms.ModelForm):
    # Adding dropdown with list of users
    createdby = forms.ModelChoiceField(queryset=User.objects.all(), label="Created by")
    notes = forms.CharField(widget=forms.Textarea, required=False)
    group = forms.ModelChoiceField(queryset=PrimerGroup.objects.all().order_by('groupnr'), label="Primer Group")
    mods5 = forms.ChoiceField(choices=mod5_choices, label="5' Modifications")
    mods3 = forms.ChoiceField(choices=mod3_choices, label="3' Modifications")
    lockrec = forms.BooleanField(label="Lock record", required=False)

    class Meta:
        model = Primer
        fields = (
        'oligonr', 'oligoname', 'sequence', 'scale', 'datefirst', 'createdby', 'group', "mods5",'mods3', 'templatetype',
        'application', 'storagelocation', 'notes','orderblockedforreview','datelast','tbo', 'ordered', 'lockrec')

        # Adding dropdown menu to Date of Order, with years ranging from 2001 to the current year
        widgets = {
            "datefirst": SelectDateWidget(years=tuple(range(date.today().year, 2000, -1)), attrs={"value": date.today}),
            'datelast': SelectDateWidget(years=tuple(range(date.today().year, 2000, -1)), attrs={"value": date.today}),
        }

class PrimerGroupForm(forms.ModelForm):
    class Meta:
        model= PrimerGroup
        fields = ('groupnr','name')

class OrderHistoryForm(forms.ModelForm):
    primers = forms.CharField(widget=forms.Textarea, required=False)
    PO_nr = forms.CharField(widget=forms.Textarea, required=False, label="PO Number")

    class Meta:
        model = OrderHistory
        fields = ('ordernr', 'PO_nr', 'orderdate', 'primers', 'orderfile')

        widgets = {
            "orderdate": SelectDateWidget(years=tuple(range(date.today().year, 2000, -1)), attrs={"value": date.today}),
        }

class UploadFileForm(forms.Form):
    file = forms.FileField(validators=[validator_excel])